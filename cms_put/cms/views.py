from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from .models import Contenido

# Create your views here.

@csrf_exempt
def get_content(request, llave):
    if request.method == 'GET':
        try:
            respuesta = Contenido.objects.get(clave=llave).valor
        except Contenido.DoesNotExist:
            respuesta = "No existe el contenido con clave -> " + llave

    elif request.method == 'PUT':
        valor = request.body.decode('utf-8')
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
            c.save()
            respuesta = "Contenido actualizado con éxito"
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
            c.save()
            respuesta = "Contenido creado con éxito"
    return HttpResponse(respuesta)