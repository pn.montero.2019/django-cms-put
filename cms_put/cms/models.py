from django.db import models
# modelos=clases
# Create your models here.
"""Modelo de datos necesario para almacenar contenidos en base de datos"""
class Contenido(models.Model):
    objects = None
    DoesNotExist = None
    clave = models.CharField(max_length=100)    # cadena texto limitada
    valor = models.TextField()   # cadena texto larga